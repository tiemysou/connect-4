// Code written by Kevin Thai - kqthai@ucdavis.edu
// and Raymond Lau - rlau@ucdavis.edu

import java.util.List;
import java.util.ArrayList;

public class AlphaBeta_KevinThai_RaymondLau extends AIModule {

    private static final int COLUMNS = 7;
    private static final int ROWS = 6;
    private static final int INITIAL_DEPTH = 3;

    Integer[] slots1 = {3, 2, 4, 1, 5, 0, 6}; // the 7 slots we can put our token in
    Integer[] slots2 = {0, 6, 1, 5, 2, 4, 3};
    Integer me, opponent;
    //24 horizontal, 21 vertical, 24 diagonal
    //69 winning combos
    Integer size = 69*8;
    Integer[] winning_combos = new Integer[size];

    public AlphaBeta_KevinThai_RaymondLau() {
        //calculate all the winning combos
        Integer index = 0;

        //calculate vertical combos
        for(int x = 0; x < COLUMNS; x++) {
            for(int y = 0; y < 3; y++) {
                for(int z = y; z < y + 4; z++) {
                    winning_combos[index] = x;
                    //System.out.print(x + ",");
                    winning_combos[index+1] = z;
                    //System.out.print(z + ", ");
                    index += 2;
                }
                //System.out.print("\n");
            }
        }

        //calculate horizontal combos
        for(int x = 0; x < 4; x++) {
            for(int y = 0; y < ROWS; y++) {
                for(int z = x; z < x + 4; z++) {
                    winning_combos[index] = z;
                    //System.out.print(z + ",");
                    winning_combos[index+1] = y;
                    //System.out.print(y + ", ");
                    index += 2;
                }
                //System.out.print("\n");
            }
        }
        //upward diagonals
        for(int z = 0; z < 4; z++) {
            for(int x = 0; x < 3; x++) {
                for(int y = 0; y < 4; y++) {
                    winning_combos[index] = y + z;
                    //System.out.print((y + z) + ",");
                    winning_combos[index+1] = x + y;
                    //System.out.print((x + y) + ", ");
                    index += 2;
                }
                //System.out.print("\n");
            }    
        }

        //downward diagonals
        for(int z = 0; z < 4; z++) {
            for(int x = 0; x < 3; x++) {
                for(int y = 5, w = 0; y > 1; y--, w++) {
                    winning_combos[index] = z + w;
                    //System.out.print(z + w +  ",");
                    winning_combos[index+1] = y - x;
                    //System.out.print((y - x) + ", ");
                    index += 2;
                }
                //System.out.print("\n");
            }    
        }
    }
    
    public void getNextMove(final GameStateModule state)
    {
        me = state.getActivePlayer();

        if (me == 1) {
            opponent = 2;
        }
        else {
            opponent = 1;
        }

            
        //try to place the first token in the middle 
        if((state.getAt(3,0)==0)) {
            this.chosenMove = 3;
            return;
        }    
        
        int max = Integer.MIN_VALUE;
        int depth = INITIAL_DEPTH;
        Integer alpha = Integer.MIN_VALUE;
        Integer beta = Integer.MAX_VALUE;
        try {
            for( ;!terminate; depth+=2)
                {
                    for(int slot : slots1)
                    {
                        if (state.canMakeMove(slot)) {
                            state.makeMove(slot);
                            int value = minimax_ab(state, depth, alpha, beta, false);
                            state.unMakeMove();
                            if(value > alpha)
                            {
                                alpha = value;
                                chosenMove = slot;
                            }
                            if(beta <= alpha) {
                                chosenMove = slot;
                                return;
                            }
                        }
                    }
                }
        }
        catch (Exception e) {}
    }
    
    //used Wikipedia algorithm
    //http://en.wikipedia.org/wiki/Minimax
    public Integer minimax_ab(GameStateModule state, Integer depth, Integer alpha, Integer beta, Boolean maximizingPlayer) {
        if(depth <= 0 || state.isGameOver()) return evaluationFunction(state);
        if(maximizingPlayer) {
            for(Integer slot : slots1) {
                if(state.canMakeMove(slot)) {
                    state.makeMove(slot);
                    Integer newAlpha = minimax_ab(state, depth - 1, alpha, beta, false);
                    alpha = Math.max(alpha, newAlpha);
                    state.unMakeMove();

                    if(beta <= alpha) break;
                }
            }
            return alpha;
        } else {
            for(Integer slot : slots2) {
                if(state.canMakeMove(slot)) {
                    state.makeMove(slot);
                    Integer newBeta = minimax_ab(state, depth - 1, alpha, beta, true);
                    beta = Math.min(beta, newBeta);
                    state.unMakeMove();

                    if(beta <= alpha) break;
                }
            }
            return beta;
        }
    }
    
    public Integer evaluationFunction(GameStateModule state) {
        if (this.terminate) {
            throw new RuntimeException("timeout");
        }

        if (state.isGameOver()) {
            if (state.getWinner() == me)
                return Integer.MAX_VALUE;             
            else if (state.getWinner() == opponent)
                return Integer.MIN_VALUE;  
            else
                return 0;   
        }
        
        // a threat is defined to be a spot that when taken would result in a win for the given player
        // player(s) that owns the threat: empty = 0, me = 1 , opponent = 2 , both me and opponent = 3
        Integer[][] threats = {
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0}};
            
        Integer result = 0;
        Integer player = state.getActivePlayer();
        // BEGIN SECTION 1 => iterate through list of winning combinations and add/subtract score based on odd/even threats 
        for(int i = 0; i < winning_combos.length; i += 8) {
            Integer meTokens = 0, opponentTokens = 0;
            List<Integer[]> empties = new ArrayList<Integer[]>(); 
            for(int n = i; n < i + 8; n += 2) {
                Integer playerAtSpot = state.getAt(winning_combos[n], winning_combos[n+1]);
                if(playerAtSpot == me) {
                    meTokens++;    
                } else if(playerAtSpot == opponent) {
                    opponentTokens++;
                } else if(playerAtSpot == 0) {
                    Integer[] empty = { winning_combos[n], winning_combos[n+1] };
                    empties.add(empty);
                }
            }
            
            
            if ((meTokens == 0) || (opponentTokens == 0)) {
                result += (meTokens - opponentTokens);
                //if there are 3 tokens, we know that there can only be one empty slot
                
                if(meTokens == 3){
                    if(empties.size() == 2) System.out.println("this shouldn't be happening"); 
                    Integer[] empty = empties.get(0);
                    Integer x = empty[0], y = empty[1];
                    if ((y > 0) && (state.getAt(x, y - 1) == 0)) { 
                        if (threats[x][y] == 0)
                            threats[x][y] = me;
                        else if (threats[x][y] == opponent)
                            threats[x][y] = 3;
                        if (me == 1){ 
                            if (y % 2 == 0) // odd threat ---> http://www.informatik.uni-trier.de/~fernau/DSL0607/Masterthesis-Viergewinnt.pdf ---> find odd and even threats ---> when reading remember that white is the first player
                                result += 10;
                        }
                        else{ 
                            if (y % 2 == 1)  // even threat 
                                result += 10;
                        }
                    }        
                    else if (player == me){  
                        return Integer.MAX_VALUE;
                    }
                } else if(opponentTokens == 3) {
                    if(empties.size() == 2) System.out.println("this shouldn't be happening");
                    Integer[] empty = empties.get(0);
                    Integer x = empty[0], y = empty[1];
                    if ((y > 0) && (state.getAt(x, y - 1) == 0)) {
                        if (threats[x][y] == 0)
                            threats[x][y] = opponent;
                        else if (threats[x][y] == me)
                            threats[x][y] = 3;
    
                        if (me == 1) { 
                            if (y % 2 == 1) { 
                                result -= 10;
                            }
                        } else { 
                            if (y % 2 == 0) 
                            {
                                result -= 10;
                            }
                        }
                    }
                    else if (player != me){ 
                        return Integer.MIN_VALUE;
                    }
                }
            } 
        }
        // END SECTION 1
        
        // BEGIN SECTION 2 => tabulate the odd/even shared/unshared threats 
        Integer p1_odd = 0, p2_odd = 0;
        Integer p1_even = 0, p2_even = 0;
        Integer odd_shared = 0, even_shared = 0;
        for(int i = 0; i < COLUMNS; i++) {
            Boolean even_threat_below_p1 = false;
            Boolean even_threat_below_p2 = false;
            Boolean odd_threat_below_p1 = false;
            Boolean odd_threat_below_p2 = false;
            Boolean shared_below = false;

            for(int j = 0; j < ROWS; j++) {
                if(threats[i][j] == 1) {
                    if(j % 2 == 0 && !even_threat_below_p1) { // odd and there doesn't exist an even threat below
                        p1_odd++;
                        odd_threat_below_p2 = true;
                    } else if(j % 2 == 1 && !odd_threat_below_p1) {
                        p1_even++;
                        even_threat_below_p2 = true;
                    }
                } else if(threats[i][j] == 2) {
                    if(j % 2 == 1 && !odd_threat_below_p2) {
                        p2_even++;
                        even_threat_below_p1 = true;
                    } else if(j % 2 == 0 && !even_threat_below_p2) {
                        p2_odd++;
                        odd_threat_below_p1 = true;
                    }
                } else if(threats[i][j] == 3) {
                    if(j % 2 == 0 && !shared_below){ // odd shared
                        odd_shared++;
                        shared_below = true;   
                    }
                    else if (j % 2 == 1 && !shared_below){ // even shared
                        even_shared++;
                        shared_below = true;   
                    }
                }
            }
        }
        // END SECTION 2
        
        // BEGIN SECTION 3 => go through 9 fundamental Connect 4 tactics for winning and add/subtract score based on results  
        Boolean rule1 = (p1_odd == p2_odd + 1);
        Boolean rule2 = (p1_odd == p2_odd && odd_shared % 2 == 1);
        Boolean rule3 = ((p1_odd + odd_shared) % 2 == 1 && p2_odd < 1);
        
        Boolean rule4 = (p2_even > 0 && ((p1_odd + odd_shared) < 1)); //p1 may not need odd_shared, not completely sure
        Boolean rule5 = (p2_odd == p1_odd + 2);
        Boolean rule6 = (p2_odd == p1_odd && (odd_shared % 2 == 0));
        Boolean rule7 = (p2_odd == p1_odd + 1 && odd_shared > 0);
        Boolean rule8 = ((p2_odd == 1 && odd_shared > 0) && p1_odd == 0);
        Boolean rule9 = ((p2_odd + odd_shared) % 2 == 0 && p1_odd == 0);
        
        if(rule1 || rule2 || rule3) {
            if(player == 1) result += 75;
            else result -= 75;
        } else if(rule4 || rule5 || rule6 || rule7 || rule8 || rule9) {
            if(player == 1) result -= 75;
            else result += 75;
        }
        
        return result;
        // END SECTION 3
    }
}